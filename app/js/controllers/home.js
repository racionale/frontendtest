'use strict';

function HomeController($scope, $http) {
    //Get data from JSON
    $http.get('PLteams.json')
        .success(function (data) {
            $scope.teams = data.teams;
        });

    //Default Grid view
    $scope.view = 'gridView';

    //See all clubs
    $scope.allClubs =function () {
        $scope.search = [];
    };

    //Open Single Team Modal
    $scope.openTeam = function (team) {
        $('#team-popup').modal('show');
        $scope.singleTeam = team;
    };

    //Close Single Team Modal
    $scope.closeModal = function () {
        $('#team-popup').modal('hide');
    };
}