'use strict';

var TestApp = angular.module('AppClubs', ['ngRoute']);

// route configurations
TestApp.config(['$routeProvider', '$locationProvider', function ($routeProvider, $locationProvider) {
	$routeProvider
		.when('/', {
			templateUrl: '../partials/home.html',
			controller: 'HomeController'
		})
		.otherwise({
			redirectTo: '/'
		});

	$locationProvider.hashPrefix('!');
}]);

// controllers
TestApp.controller('HomeController', ['$scope','$http', HomeController]);